﻿using UnityEngine.UI;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject scoreText;
    public GameObject timerText;

    public DateTime timer;
    public TimeSpan timeUntilFinish = TimeSpan.FromMinutes(1);
    public GameObject resultPanel;
    public GameObject resultText;

    Grid grid;


    void Start()
    {
        resultPanel.SetActive(false);
        timer = DateTime.Now;
        grid = GameObject.FindObjectOfType<Grid>();

    }

    void Update()
    {
        scoreText.GetComponent<Text>().text = "" + grid.point;

        timeUntilFinish -= DateTime.Now - timer;
        timer = DateTime.Now;
        timerText.GetComponent<Text>().text = String.Format(" {0:00}:{1:00}", timeUntilFinish.Minutes, timeUntilFinish.Seconds);
        if(timeUntilFinish <= TimeSpan.Zero)
        {
            timerText.GetComponent<Text>().text = " 00:00 ";
            resultPanel.SetActive(true);
            resultText.GetComponent<Text>().text = "Score Anda : " + grid.point;
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);

    }
}
